/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CAN1_SHDN_Pin GPIO_PIN_2
#define CAN1_SHDN_GPIO_Port GPIOE
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define GPIO_A1_Pin GPIO_PIN_2
#define GPIO_A1_GPIO_Port GPIOF
#define GPIO_B1_Pin GPIO_PIN_3
#define GPIO_B1_GPIO_Port GPIOF
#define GPIO_A17_Pin GPIO_PIN_4
#define GPIO_A17_GPIO_Port GPIOF
#define CC_ALERT_Pin GPIO_PIN_2
#define CC_ALERT_GPIO_Port GPIOC
#define CC_PWRDN_Pin GPIO_PIN_3
#define CC_PWRDN_GPIO_Port GPIOC
#define PEN2_Pin GPIO_PIN_5
#define PEN2_GPIO_Port GPIOA
#define PEN3_Pin GPIO_PIN_6
#define PEN3_GPIO_Port GPIOA
#define PEN1_Pin GPIO_PIN_7
#define PEN1_GPIO_Port GPIOA
#define YN_M_Pin GPIO_PIN_0
#define YN_M_GPIO_Port GPIOB
#define ZN_MSU_Pin GPIO_PIN_1
#define ZN_MSU_GPIO_Port GPIOB
#define XN_MSU_Pin GPIO_PIN_2
#define XN_MSU_GPIO_Port GPIOB
#define MAG_DR_Pin GPIO_PIN_1
#define MAG_DR_GPIO_Port GPIOG
#define GPS_EN_Pin GPIO_PIN_7
#define GPS_EN_GPIO_Port GPIOE
#define GPS_1SEC_Pin GPIO_PIN_8
#define GPS_1SEC_GPIO_Port GPIOE
#define Coil_En_Pin GPIO_PIN_15
#define Coil_En_GPIO_Port GPIOE
#define PLD_POWER_Pin GPIO_PIN_10
#define PLD_POWER_GPIO_Port GPIOB
#define SPI2_NCC0_Pin GPIO_PIN_12
#define SPI2_NCC0_GPIO_Port GPIOB
#define GPIO_A25_Pin GPIO_PIN_8
#define GPIO_A25_GPIO_Port GPIOD
#define GPIO_B25_Pin GPIO_PIN_9
#define GPIO_B25_GPIO_Port GPIOD
#define SPI1_CS_Pin GPIO_PIN_5
#define SPI1_CS_GPIO_Port GPIOG
#define USB_PowerSwitchOn_Pin GPIO_PIN_6
#define USB_PowerSwitchOn_GPIO_Port GPIOG
#define STLK_RX_Pin GPIO_PIN_7
#define STLK_RX_GPIO_Port GPIOG
#define STLK_TX_Pin GPIO_PIN_8
#define STLK_TX_GPIO_Port GPIOG
#define USB_SOF_Pin GPIO_PIN_8
#define USB_SOF_GPIO_Port GPIOA
#define USB_VBUS_Pin GPIO_PIN_9
#define USB_VBUS_GPIO_Port GPIOA
#define USB_ID_Pin GPIO_PIN_10
#define USB_ID_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_11
#define USB_DM_GPIO_Port GPIOA
#define USB_DP_Pin GPIO_PIN_12
#define USB_DP_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define BB_Pin GPIO_PIN_15
#define BB_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define DEBG_LEDU_Pin GPIO_PIN_7
#define DEBG_LEDU_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define BB_ENABLE 					HAL_GPIO_WritePin(BB_GPIO_Port, BB_Pin, GPIO_PIN_SET);
#define BB_DISABLE 					HAL_GPIO_WritePin(BB_GPIO_Port, BB_Pin, GPIO_PIN_RESET);
#define COIL_ENABLE 				HAL_GPIO_WritePin(Coil_En_GPIO_Port, Coil_En_Pin, GPIO_PIN_SET);
#define COIL_DISABLE 				HAL_GPIO_WritePin(Coil_En_GPIO_Port, Coil_En_Pin, GPIO_PIN_RESET);
#define ADCS_PWR_ENABLE 	     	HAL_GPIO_WritePin(BB_GPIO_Port, BB_Pin, GPIO_PIN_SET);
#define ADCS_PWR_DISABLE		    HAL_GPIO_WritePin(BB_GPIO_Port, BB_Pin, GPIO_PIN_RESET);
#define GPS_ENABLE 			        HAL_GPIO_WritePin(GPS_EN_GPIO_Port, GPS_EN_Pin, GPIO_PIN_SET);
#define GPS_DISABLE 			    HAL_GPIO_WritePin(GPS_EN_GPIO_Port, GPS_EN_Pin, GPIO_PIN_RESET);

#define PAC1934_SLOW_Pin 			CC_ALERT_Pin
#define PAC1934_SLOW_GPIO_Port 		CC_ALERT_GPIO_Port
#define PAC1934_PWRDN_Pin 			CC_PWRDN_Pin
#define PAC1934_PWRDN_GPIO_Port 	CC_PWRDN_GPIO_Port

#define DEBUG_LED_ON                HAL_GPIO_WritePin(DEBG_LEDU_GPIO_Port, DEBG_LEDU_Pin, GPIO_PIN_SET);
#define DEBUG_LED_OFF               HAL_GPIO_WritePin(DEBG_LEDU_GPIO_Port, DEBG_LEDU_Pin, GPIO_PIN_RESET);
#define DEBUG_TOGGLE                HAL_GPIO_TogglePin(DEBG_LEDU_GPIO_Port, DEBG_LEDU_Pin);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
