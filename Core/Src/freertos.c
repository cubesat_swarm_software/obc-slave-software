/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
#include "log_utils.h"
#include "log_sd.h"
#include "logbook.h"
#include "repo_cmd.h"
#include "ring_buffer.h"
#include "unican.h"
#include "status.h"
#include "default_settings.h"
#include "iwdg.h"
#include "errors.h"
#include "telemetry.h"
#include "../../Application/Drivers/Imu_sens.h"
#include "../../Application/Drivers/GPS.h"
#include "sd.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/*

*/

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
struct unican_handler ISL_dispatcher;
struct unican_handler command_dispatcher;
SemaphoreHandle_t      I2C_mtx;
StaticSemaphore_t      I2C_mtx_buffer;

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
uint32_t defaultTaskBuffer[ 128 ];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId dispatcherTaskHandle;
uint32_t dispatcherTaskBuffer[ 512 ];
osStaticThreadDef_t dispatcherTaskControlBlock;
osThreadId telemetryTaskHandle;
uint32_t telemetryTaskBuffer[ 768 ];
osStaticThreadDef_t telemetryTaskControlBlock;
osThreadId hearbeatTaskHandle;
uint32_t hearbeatTaskBuffer[ 128 ];
osStaticThreadDef_t hearbeatTaskControlBlock;
osThreadId ISLTaskHandle;
uint32_t ISLTaskBuffer[ 256 ];
osStaticThreadDef_t ISLTaskControlBlock;
osThreadId IWDGTaskHandle;
uint32_t IWDGTaskBuffer[ 128 ];
osStaticThreadDef_t IWDGTaskControlBlock;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartDispatcherTask(void const * argument);
void StartTelemetryTask(void const * argument);
void StartHearbeatTask(void const * argument);
void ISLTaskStart(void const * argument);
void StartTaskIWDG(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
	I2C_mtx = xSemaphoreCreateMutexStatic(&I2C_mtx_buffer);
	xSemaphoreGive(I2C_mtx);

  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */

  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadStaticDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128, defaultTaskBuffer, &defaultTaskControlBlock);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of dispatcherTask */
  osThreadStaticDef(dispatcherTask, StartDispatcherTask, osPriorityHigh, 0, 512, dispatcherTaskBuffer, &dispatcherTaskControlBlock);
  dispatcherTaskHandle = osThreadCreate(osThread(dispatcherTask), NULL);

  /* definition and creation of telemetryTask */
  osThreadStaticDef(telemetryTask, StartTelemetryTask, osPriorityNormal, 0, 768, telemetryTaskBuffer, &telemetryTaskControlBlock);
  telemetryTaskHandle = osThreadCreate(osThread(telemetryTask), NULL);

  /* definition and creation of hearbeatTask */
  osThreadStaticDef(hearbeatTask, StartHearbeatTask, osPriorityNormal, 0, 128, hearbeatTaskBuffer, &hearbeatTaskControlBlock);
  hearbeatTaskHandle = osThreadCreate(osThread(hearbeatTask), NULL);

  /* definition and creation of ISLTask */
  osThreadStaticDef(ISLTask, ISLTaskStart, osPriorityNormal, 0, 256, ISLTaskBuffer, &ISLTaskControlBlock);
  ISLTaskHandle = osThreadCreate(osThread(ISLTask), NULL);

  /* definition and creation of IWDGTask */
  osThreadStaticDef(IWDGTask, StartTaskIWDG, osPriorityHigh, 0, 128, IWDGTaskBuffer, &IWDGTaskControlBlock);
  IWDGTaskHandle = osThreadCreate(osThread(IWDGTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  unican_add_handler(UNICAN_HANDLERID_ISL, &ISL_dispatcher);
  unican_add_handler(UNICAN_HANDLERID_COMMAND, &command_dispatcher);
  //osTimerStart(heartbeatHandle, 3000);
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
	const char * tag = "DefaultTask";
	const TickType_t xFrequency = 1000;
	TickType_t xLastWakeTime = xTaskGetTickCount();
	LOGI(tag,"Start");
	for(;;)
	{
		DEBUG_TOGGLE
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}


  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartDispatcherTask */

/**
* @brief Function implementing the dispatcherTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDispatcherTask */
void StartDispatcherTask(void const * argument)
{
  /* USER CODE BEGIN StartDispatcherTask */
  /* Infinite loop */
	static const char *tag = "OBC dispatcher task";
	LOGI(tag,"Start");
	struct cmd_t cmd_new;
	uint8_t cmd_table_id;
	uint16_t cmd_res;
	struct status* dev_status = status_get();
	for(;;)
	{
		if (unican_receive(&command_dispatcher, portMAX_DELAY)) {
			uint16_t res_check = cmd_check(command_dispatcher.packet.msg_id,command_dispatcher.packet.data_len,&cmd_table_id);
			if(res_check==CMD_OK){
				cmd_new.id_cmd = cmd_table_id;
				cmd_new.sender = command_dispatcher.packet.sender;
				cmd_new.args   = (command_dispatcher.packet.data_len>0)?command_dispatcher.packet.data:NULL;
				cmd_res = cmd_run(cmd_new.id_cmd,cmd_new.sender, cmd_new.args);
				if(cmd_res==0){
					unican_ack(cmd_new.sender,command_dispatcher.packet.msg_id);
				}
				else{
					unican_nack(cmd_new.sender,command_dispatcher.packet.msg_id,cmd_res);
				}
			}
			else if(res_check==CMD_NOT_FOUND){
				unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_ANKNOWN_CMD);
				LOGI(tag,"UNKNOWN COMMAND");
			}
			else{
				unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_CMD_WRONG_PARAM);
				LOGI(tag,"WRONG PARAM");
			}
			if (1){
			  char result[50] = "";
			  sprintf(result, "Cmd %d check %d, res %d \n",command_dispatcher.packet.msg_id,res_check,cmd_res);
			  log_sd_str("Events.txt", &result, strlen(result));
			}
		}
	}

  /* USER CODE END StartDispatcherTask */
}

/* USER CODE BEGIN Header_StartTelemetryTask */
/**
* @brief Function implementing the telemetryTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTelemetryTask */
void StartTelemetryTask(void const * argument)
{
  /* USER CODE BEGIN StartTelemetryTask */
  /* Infinite loop */
	static const char *tag = "OBC telemerty";
	TickType_t xFrequency = DEFAULT_TEL_REG_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	struct status* dev_status = status_get();
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 1000)== pdTRUE){
		  telemerty_regylar();
		  struct telemerty_reg *tel_reg = telemerty_regular_get();
		  if(dev_status->tel_sd_log){
			  log_sd_int_array("Tempetarures.txt",tel_reg->temperature_param,T_SENS_OBC_LAST-T_SENS_OBC_FIRST+1)||
			  log_sd_float_array("Imu.txt",tel_reg->imu_param,IMU_PARAM_LAST - IMU_PARAM_FIRST+1)||
			  log_sd_int_array("Current.txt",tel_reg->current_param,UI_SENSORS_OBC_LAST - UI_SENSORS_OBC_FIRST+1)||
			  log_sd_int_array("Voltage.txt",tel_reg->voltage_param,UI_SENSORS_OBC_LAST - UI_SENSORS_OBC_FIRST+1);
			  LOGI(tag,"LOG TELEMETRY");
		  }
		  if(dev_status->tel_UHF_en){
			  struct unican_message msg;
			  msg.data = (uint8_t *)tel_reg;
			  msg.unican_length = 3;
			  msg.unican_msg_id = 17029;
			  msg.unican_address_to = 0x1;
			  unican_send_msg(&msg);
			  LOGI(tag,"SEND TELEMETRY");
		  }
		  xSemaphoreGive(I2C_mtx);
		  vTaskDelayUntil( &xLastWakeTime, xFrequency );
		}
	}


  /* USER CODE END StartTelemetryTask */
}

/* USER CODE BEGIN Header_StartHearbeatTask */
/**
* @brief Function implementing the hearbeatTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartHearbeatTask */
void StartHearbeatTask(void const * argument)
{
  /* USER CODE BEGIN StartHearbeatTask */
  /* Infinite loop */
	static const char *tag = "Hearbeat";
	TickType_t xFrequency = DEFOULT_HEATBEAT_PEROOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
	  unican_hearbeat(0x18);
	  vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartHearbeatTask */
}

/* USER CODE BEGIN Header_ISLTaskStart */
//struct unican_handler ISL_dispatcher;
/**
* @brief Function implementing the ISLTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ISLTaskStart */
void ISLTaskStart(void const * argument)
{
  /* USER CODE BEGIN ISLTaskStart */
  /* Infinite loop */
	static const char *tag = "ISL slave Task";
	LOGI(tag,"Start");
	struct status* dev_status = status_get();
	for(;;)
	{
		if (unican_receive(&ISL_dispatcher, portMAX_DELAY)) {
			LOGI(tag, "GOT ISL PACKAGE");
			struct unican_message msg;
			msg.unican_length = 3;
			msg.unican_msg_id = 11111;
			msg.unican_address_to = DEFAULT_CAN_ISL_FRIEND_DEVICE_ID;
			dev_status->tel_UHF_en = 0;
			unican_send_msg(&msg);
			dev_status->tel_UHF_en = 1;
		}
	}

  /* USER CODE END ISLTaskStart */
}

/* USER CODE BEGIN Header_StartTaskIWDG */
/**
* @brief Function implementing the IWDGTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskIWDG */
void StartTaskIWDG(void const * argument)
{
  /* USER CODE BEGIN StartTaskIWDG */
  /* Infinite loop */
	static const char *tag = "IWDG";
	TickType_t xFrequency = DEFAULT_IWDG_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		HAL_IWDG_Refresh(&hiwdg);
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartTaskIWDG */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
