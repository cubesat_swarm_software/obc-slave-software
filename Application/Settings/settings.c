#include "settings.h"
#include "string.h"
#include "stdio.h"
#include "flash.h"

static struct settings device;

struct settings *settings_get(){
	return &device;
}

void settings_load_default(void)
{
	memset( &device, 0x00, sizeof(struct settings));
	sprintf( device.dev_serial, "%s", DEFAULT_DEVICE_SERIAL);
	device.fw_version = DEFAULT_FW_VERSION;
	device.can_device_id = DEFAULT_CAN_DEVICE_ID;
	device.can_isl_friend_address = DEFAULT_CAN_ISL_FRIEND_DEVICE_ID;
	device.tm_reg_period = DEFAULT_TEL_REG_PERIOD;

	//add other parameters
}

uint16_t settings_store_to_flash(){
	uint16_t res = flash_erase(FLASH_SETTINGS_ADDRESS, 1);
	if(res==0){
		return flash_save((void*)(&device), FLASH_SETTINGS_ADDRESS, sizeof(device ));
	}
	return res;
}

uint16_t setttings_read_from_flash() {
    uint32_t *dest_adr = (uint32_t *)&device;
	for (uint16_t i=0; i < SETTINGS_WORD_CNT; i=i+1) {
		*(dest_adr + i) = *( uint32_t*)(FLASH_SETTINGS_ADDRESS + 4*i);
	}
	return 0;
}

