/*
 * TMP100.c
 *
 *  Created on: Sep 30, 2020
 *      Author: Данил
 */


#include "TMP100.h"
#include "i2c.h"
uint8_t TMP100_WriteRegister (uint8_t address, uint8_t reg, uint8_t value)
{
	if (HAL_I2C_Mem_Write(&hi2c1, address << 1, reg, 1, &value, 1, 1000)==HAL_OK)
		return 0;
	return 1;
}

uint8_t TMP100_ReadRegister(uint8_t address, uint8_t reg, uint16_t *value)
{
	uint8_t i2c_state;
	uint8_t buf[2] = {reg, 0};
	while((i2c_state = HAL_I2C_Master_Transmit(&hi2c1, address << 1, buf, 1, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;
	//HAL_Delay(5);
	while((i2c_state = HAL_I2C_Master_Receive(&hi2c1, address << 1, buf, 2, 1000)) == HAL_BUSY);
	if (i2c_state == HAL_ERROR)
		return 1;

	*value = (buf[0] << 8) + buf[1];
	return 0;
}

uint8_t TMP100_Init(uint8_t address)
{
	uint8_t data = 0;

	if (TMP100_WriteRegister (address, TMP100_CONF_REG, data))
		return 1;
	return 0;
}

uint8_t TMP100_GetTemperature(uint8_t address, int16_t *value)
{
	int16_t buffer;

	if (TMP100_ReadRegister(address, TMP100_TEMP_REG, &buffer))
		return 1;
	*value = (int16_t) ((buffer >> 4) >> 4);
	return 0;
}
