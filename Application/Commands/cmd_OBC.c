/*
 * cmd_OBC.c
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */

#include "cmd_OBC.h"
#include "log_utils.h"
#include "settings.h"
#include "status.h"
static const char *tag = "cmd_OBC";
extern SemaphoreHandle_t xSemaphore_ISL;

int prob_cmd(uint8_t sender, uint8_t *data) {
	LOGI(tag, "I AM OBC");
    return 0;
}

int turn_off_telemetry_com(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_UHF_en = 0;
	return 0;
}

int turn_on_telemetry_com(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_UHF_en = 1;
	return 0;
}

int turn_off_telemerty_logging(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_sd_log = 0;
	return 0;
}

int turn_on_telemerty_logging(uint8_t sender, uint8_t *data){
	struct status* dev_status = status_get();
	dev_status->tel_sd_log = 1;
	return 0;
}


