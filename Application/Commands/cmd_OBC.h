/*
 * cmd_OBC.h
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef INC_CMD_OBC_H_
#define INC_CMD_OBC_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"
int prob_cmd(uint8_t sender, uint8_t *data) ;
int turn_off_telemetry_com(uint8_t sender, uint8_t *data);
int turn_on_telemetry_com(uint8_t sender, uint8_t *data);
int turn_on_telemerty_logging(uint8_t sender, uint8_t *data);
int turn_off_telemerty_logging(uint8_t sender, uint8_t *data);

#endif /* INC_CMD_OBC_H_ */
