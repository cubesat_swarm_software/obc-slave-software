/*
 * power.h
 *
 *  Created on: 29 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_POWER_H_
#define COMMANDS_POWER_H_

#include "main.h"

typedef enum {CH_BB=0, CH_COLS=1, CH_GPS =2} channels;
typedef enum {OFF=0, ON=1} channel_states;

int power_sw_on(uint8_t channel);

int power_sw_off(uint8_t channel);


#endif /* COMMANDS_POWER_H_ */
