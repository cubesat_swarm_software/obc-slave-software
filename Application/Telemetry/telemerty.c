/*
 * telemerty.c
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: Ilia
 */
#include  "telemetry.h"
#include "log_utils.h"
#include "status.h"
#include "errors.h"
#include "logbook.h"
#include "message_buffer.h"
#include "../../Application/Drivers/INA219.h"
#include "../../Application/Drivers/DS1631.h"
#include "../../Application/Drivers/PAC1934.h"
#include "../../Application/Drivers/Imu_sens.h"
#include "../../Application/Drivers/TMP100.h"
static const char * tag = "telemerty";
static struct telemerty_reg tel_reg;
static uint8_t sens_err[SENS_OBC_COUNT] = {0};
void telemerty_regylar(){
	  PAC1934_RefreshV(&i2c_PAC1934);
	  PAC1934_RefreshV(&i2c_SATBUS);
	  uint16_t error_id;
	  struct status* dev_status = status_get();
	  if (dev_status->sens_err[SENS_OBC_TERM100NA_COILXY]==0){
		  error_id= TMP100_GetTemperature(TMP100_COILXY,&tel_reg.temperature_param[T_SENS_COILXY - T_SENS_FIRST]);
		  dev_status->sens_err[SENS_OBC_TERM100NA_COILXY] = error_id==0? 0: ERR_TMP100_GETTING;
	  }
	  if (dev_status->sens_err[SENS_OBC_TERM100NA_COILZ]==0){
		  error_id = TMP100_GetTemperature(TMP100_COILZ,&tel_reg.temperature_param[T_SENS_COILZ - T_SENS_FIRST]);
		  dev_status->sens_err[SENS_OBC_TERM100NA_IMU] = error_id==0? 0: ERR_TMP100_GETTING;
	  }
	  if (dev_status->sens_err[SENS_OBC_TERM100NA_IMU]==0){
		  error_id = TMP100_GetTemperature(TMP100_IMU,&tel_reg.temperature_param[T_SENS_IMU - T_SENS_FIRST]);
		  dev_status->sens_err[SENS_OBC_TERM100NA_IMU] = error_id==0? 0: ERR_TMP100_GETTING;
	  }
	  if (dev_status->sens_err[SENS_OBC_TERM100NA_SW_3V3]==0){
		  error_id = TMP100_GetTemperature(TMP100_SW_3V3,&tel_reg.temperature_param[T_SENS_SW_3V3 - T_SENS_FIRST]);
		  dev_status->sens_err[SENS_OBC_TERM100NA_SW_3V3] = error_id==0? 0: ERR_TMP100_GETTING;
	  }

	  if (dev_status->sens_err[SENS_OBC_INA219_BB_VCC]==0){
		  error_id = INA219_GetUI(&i2c_INA219,INA219_BB_VCC, &tel_reg.voltage_param[BB_VCC - UI_SENSORS_FIRST], &tel_reg.current_param[BB_VCC - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_BB_VCC] = error_id==0? 0: ERR_INA219_BB_VCC;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_COIL_VCC]==0){
		  error_id = INA219_GetUI(&i2c_INA219,INA219_COIL_VCC, &tel_reg.voltage_param[COIL_VCC - UI_SENSORS_FIRST], &tel_reg.current_param[COIL_VCC - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_COIL_VCC] = error_id==0? 0: ERR_INA219_COIL_VCC;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_OBC_3V3]==0){
		  error_id= INA219_GetUI(&i2c_INA219,INA219_OBC_3V3, &tel_reg.voltage_param[OBC_3V3 - UI_SENSORS_FIRST], &tel_reg.current_param[OBC_3V3 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_OBC_3V3] = error_id==0? 0: ERR_INA219_OBC_3V3;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_CHRG1]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_CHRG1, &tel_reg.voltage_param[CHRG1 - UI_SENSORS_FIRST], &tel_reg.current_param[CHRG1 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_CHRG1] = error_id==0? 0: ERR_INA219_CHRG1;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_CHRG2]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_CHRG2, &tel_reg.voltage_param[CHRG2 - UI_SENSORS_FIRST], &tel_reg.current_param[CHRG2 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_CHRG1] = error_id==0? 0: ERR_INA219_CHRG2;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_VBAT3]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_VBAT3, &tel_reg.voltage_param[VBAT3 - UI_SENSORS_FIRST], &tel_reg.current_param[VBAT3 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_VBAT3] = error_id==0? 0: ERR_INA219_VBAT3;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_BATP]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_BATP, &tel_reg.voltage_param[BATP - UI_SENSORS_FIRST], &tel_reg.current_param[BATP - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_VBAT3] = error_id==0? 0: ERR_INA219_BATP;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_VBAT2]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_VBAT2, &tel_reg.voltage_param[VBAT2 - UI_SENSORS_FIRST], &tel_reg.current_param[VBAT2 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_VBAT2] = error_id==0? 0: ERR_INA219_VBAT2;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_BATC1]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_BATC1, &tel_reg.voltage_param[BATC1 - UI_SENSORS_FIRST], &tel_reg.current_param[BATC1 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_BATC1] = error_id==0? 0: ERR_INA219_BATC1;
	  }
	  if (dev_status->sens_err[SENS_OBC_INA219_BATC2]==0){
		  error_id = INA219_GetUI(&i2c_SATBUS,INA219_BATC2, &tel_reg.voltage_param[BATC2 - UI_SENSORS_FIRST], &tel_reg.current_param[BATC2 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_INA219_BATC2] = error_id==0? 0: ERR_INA219_BATC2;
	  }


	  if (dev_status->sens_err[SENS_OBC_PAC1934_COL_X]==0){
		  error_id = PAC1934_GetUI(&i2c_PAC1934,PAC1934_COIL_X, &tel_reg.voltage_param[COIL_X - UI_SENSORS_FIRST], &tel_reg.current_param[COIL_X - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_PAC1934_COL_X] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_OBC_PAC1934_COL_Y]==0){
		  error_id = PAC1934_GetUI(&i2c_PAC1934,PAC1934_COIL_Y, &tel_reg.voltage_param[COIL_Y - UI_SENSORS_FIRST], &tel_reg.current_param[COIL_Y - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_PAC1934_COL_Y] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_OBC_PAC1934_COL_Z]==0){
		  error_id = PAC1934_GetUI(&i2c_PAC1934,PAC1934_COIL_Z, &tel_reg.voltage_param[COIL_Z - UI_SENSORS_FIRST], &tel_reg.current_param[COIL_Z - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_OBC_PAC1934_COL_Z] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_EPS_PAC1934_CH1]==0){
		  error_id =  PAC1934_GetUI_AVG(&i2c_SATBUS, PAC1934_PCH1, &tel_reg.voltage_param[PCH1 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH1 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_EPS_PAC1934_CH1] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_EPS_PAC1934_CH2]==0){
		  error_id =   PAC1934_GetUI_AVG(&i2c_SATBUS, PAC1934_PCH2, &tel_reg.voltage_param[PCH2 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH2 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_EPS_PAC1934_CH2] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_EPS_PAC1934_CH3]==0){
		  error_id =   PAC1934_GetUI_AVG(&i2c_SATBUS, PAC1934_PCH3, &tel_reg.voltage_param[PCH3 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH3 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_EPS_PAC1934_CH2] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }
	  if (dev_status->sens_err[SENS_EPS_PAC1934_CH4]==0){
		  error_id =   PAC1934_GetUI_AVG(&i2c_SATBUS, PAC1934_PCH4, &tel_reg.voltage_param[PCH4 - UI_SENSORS_FIRST], &tel_reg.current_param[PCH4 - UI_SENSORS_FIRST]);
		  dev_status->sens_err[SENS_EPS_PAC1934_CH4] = error_id==0? 0: ERR_PAC1934_GETTING;
	  }


	  if (dev_status->sens_err[SENS_OBC_LSM9DS1_MAG]==0){
		  error_id = Imu_read_mag(	&tel_reg.imu_param[MAGN_X - IMU_PARAM_FIRST], &tel_reg.imu_param[MAGN_Y - IMU_PARAM_FIRST], &tel_reg.imu_param[MAGN_Z - IMU_PARAM_FIRST]);
		  dev_status->sens_err[SENS_OBC_LSM9DS1_MAG] = error_id==0? 0: ERR_IMU_MAG;
	  }
	  if (dev_status->sens_err[SENS_OBC_LSM9DS1_GYRO]==0){
		  Imu_read_gyro_accel(&tel_reg.imu_param[GYRO_X - IMU_PARAM_FIRST], &tel_reg.imu_param[GYRO_Y - IMU_PARAM_FIRST], &tel_reg.imu_param[GYRO_Z - IMU_PARAM_FIRST],
							  &tel_reg.imu_param[ASCIL_X - IMU_PARAM_FIRST],&tel_reg.imu_param[ASCIL_Y - IMU_PARAM_FIRST],&tel_reg.imu_param[ASCIL_Z - IMU_PARAM_FIRST]);
		  dev_status->sens_err[SENS_OBC_LSM9DS1_GYRO] = error_id==0? 0: ERR_IMU_GYRO;
	  }

	  time_get(&tel_reg.time);

	  LOGI(tag, "Tempetarures:");
	  LOG_int16_array(tel_reg.temperature_param,4);
	  LOGI(tag, "CurrenT: ");
	  LOG_int16_array(tel_reg.current_param,16);
	  LOGI(tag, "Voltage: ");
	  LOG_int16_array(tel_reg.voltage_param,16);
	  LOGI(tag, "IMU: ");
	  LOG_float_array(tel_reg.imu_param,9);
	  for (uint8_t i=0;i<SENS_OBC_COUNT;i++){
		  if(dev_status->sens_err[i]!=sens_err[i]){
			struct Logbook_t logbook;
			logbook.error = dev_status->sens_err[i];
			logbook_write(&logbook);
		  }
		  sens_err[i] = dev_status->sens_err[i];
	  }
	  for (uint8_t i=0;i<SENS_OBC_COUNT;i++){
		  if(dev_status->sens_err[i]!=0){
			  LOGI(tag, "ERRORS IN SENSORS");
			  LOG_uint8_array(dev_status->sens_err, SENS_OBC_COUNT);
			  break;
		  }
		  if(i==SENS_OBC_COUNT-1){
			  LOGI(tag, "ALL SENSORS ARE OK");
		  }
	  }
}

struct telemerty_reg * telemerty_regular_get(){
	return &tel_reg;
}
