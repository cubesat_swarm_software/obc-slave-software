/*
 * telemetry.h
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_TELEMETRY_H_
#define COMMANDS_TELEMETRY_H_

/*
 * telemerty.h
 *
 *  Created on: 17 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef COMMANDS_TELEMERTY_H_
#define COMMANDS_TELEMERTY_H_
#include "stm32l4xx_hal.h"
#include "board.h"
#include "time.h"

#pragma push(pack)
#pragma pack(1)

struct telemerty_reg {
	int16_t  temperature_param [T_SENS_OBC_LAST - T_SENS_OBC_FIRST + 1];
	uint16_t current_param     [UI_SENSORS_LAST - UI_SENSORS_FIRST + 1];
	uint16_t voltage_param     [UI_SENSORS_LAST - UI_SENSORS_FIRST + 1];
	float    imu_param         [IMU_PARAM_LAST  - IMU_PARAM_FIRST + 1];
	struct time_board time;
};
#pragma pop(pack)

void telemerty_regylar();

struct telemerty_reg * telemerty_regular_get();

#endif /* COMMANDS_TELEMERTY_H_ */


#endif /* COMMANDS_TELEMETRY_H_ */
