/*
 * log_utils.c
 *
 *  Created on: 19 ���. 2021 �.
 *      Author: Ilia
 */

#ifndef INC_LOG_UTILS_C_
#define INC_LOG_UTILS_C_
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"
void log_utils_init(UART_HandleTypeDef * uart_);
void LOGI(const char* tag, const char *msg);
void LOG_float_array(const float* array,uint8_t n);
void LOG_int16_array(const int16_t *array,uint8_t n);
void LOG_uint8_array(const uint8_t *array,uint8_t n);
void i2c_Scan(I2C_HandleTypeDef *hi2c);
#endif /* INC_LOG_UTILS_C_ */
