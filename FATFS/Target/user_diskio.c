/* USER CODE BEGIN Header */
/**
 ******************************************************************************
  * @file    user_diskio.c
  * @brief   This file includes a diskio driver skeleton to be completed by the user.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
 /* USER CODE END Header */

#ifdef USE_OBSOLETE_USER_CODE_SECTION_0
/*
 * Warning: the user section 0 is no more in use (starting from CubeMx version 4.16.0)
 * To be suppressed in the future.
 * Kept to ensure backward compatibility with previous CubeMx versions when
 * migrating projects.
 * User code previously added there should be copied in the new user sections before
 * the section contents can be deleted.
 */
/* USER CODE BEGIN 0 */
/* USER CODE END 0 */
#endif

/* USER CODE BEGIN DECL */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "ff_gen_drv.h"
#include "sd.h"

#include "fatfs.h"
#include "stm32l4xx_hal.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
#include "time.h"
//UART_HandleTypeDef huart1;
extern UART_HandleTypeDef * muart;
uint8_t sect[256];
extern char str1[60];
uint32_t byteswritten,bytesread;
uint8_t result;
extern char USERPath[4]; /* logical drive path */

FATFS SDFatFs;
FATFS *fs;
FIL MyFile;
FILINFO fileInfo;
char *fn;
DIR dir;
DWORD fre_clust, fre_sect, tot_sect;

/* Disk status */
static volatile DSTATUS Stat = STA_NOINIT;
extern sd_info_ptr sdinfo;
/* USER CODE END DECL */

/* Private function prototypes -----------------------------------------------*/
DSTATUS USER_initialize (BYTE pdrv);
DSTATUS USER_status (BYTE pdrv);
DRESULT USER_read (BYTE pdrv, BYTE *buff, DWORD sector, UINT count);
#if _USE_WRITE == 1
  DRESULT USER_write (BYTE pdrv, const BYTE *buff, DWORD sector, UINT count);
#endif /* _USE_WRITE == 1 */
#if _USE_IOCTL == 1
  DRESULT USER_ioctl (BYTE pdrv, BYTE cmd, void *buff);
#endif /* _USE_IOCTL == 1 */

Diskio_drvTypeDef  USER_Driver =
{
  USER_initialize,
  USER_status,
  USER_read,
#if  _USE_WRITE
  USER_write,
#endif  /* _USE_WRITE == 1 */
#if  _USE_IOCTL == 1
  USER_ioctl,
#endif /* _USE_IOCTL == 1 */
};

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initializes a Drive
  * @param  pdrv: Physical drive number (0..)
  * @retval DSTATUS: Operation status
  */
DSTATUS USER_initialize (
	BYTE pdrv           /* Physical drive nmuber to identify the drive */
)
{
  /* USER CODE BEGIN INIT */
    SD_PowerOn();
	if(sd_ini()==0) {Stat &= ~STA_NOINIT;}
	return Stat;
  /* USER CODE END INIT */
}

/**
  * @brief  Gets Disk Status
  * @param  pdrv: Physical drive number (0..)
  * @retval DSTATUS: Operation status
  */
DSTATUS USER_status (
	BYTE pdrv       /* Physical drive number to identify the drive */
)
{
  /* USER CODE BEGIN STATUS */
	if (pdrv) return STA_NOINIT;
	return Stat;
  /* USER CODE END STATUS */
}

/**
  * @brief  Reads Sector(s)
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data buffer to store read data
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to read (1..128)
  * @retval DRESULT: Operation result
  */
DRESULT USER_read (
	BYTE pdrv,      /* Physical drive nmuber to identify the drive */
	BYTE *buff,     /* Data buffer to store read data */
	DWORD sector,   /* Sector address in LBA */
	UINT count      /* Number of sectors to read */
)
{
  /* USER CODE BEGIN READ */
	if (pdrv || !count) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	if (!(sdinfo.type & 4)) sector *= 512; /* Convert to byte address if needed */
	if (count == 1) /* Single block read */
	{
	  SD_Read_Block(buff,sector); //Р РЋРЎвЂЎР С�?РЎвЂљР В°Р ВµР С? Р В±Р В»Р С•Р С” Р Р† Р В±РЎС“РЎвЂћР ВµРЎР‚
	  count = 0;
	}
	else /* Multiple block read */
	{
	}
	SPI_Release();
	return count ? RES_ERROR : RES_OK;
	return RES_OK;
  /* USER CODE END READ */
}

/**
  * @brief  Writes Sector(s)
  * @param  pdrv: Physical drive number (0..)
  * @param  *buff: Data to be written
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to write (1..128)
  * @retval DRESULT: Operation result
  */
#if _USE_WRITE == 1
DRESULT USER_write (
	BYTE pdrv,          /* Physical drive nmuber to identify the drive */
	const BYTE *buff,   /* Data to be written */
	DWORD sector,       /* Sector address in LBA */
	UINT count          /* Number of sectors to write */
)
{
  /* USER CODE BEGIN WRITE */
  /* USER CODE HERE */
	if (pdrv || !count) return RES_PARERR;

	if (Stat & STA_NOINIT) return RES_NOTRDY;

	if (Stat & STA_PROTECT) return RES_WRPRT;

	if (!(sdinfo.type & 4)) sector *= 512; /* Convert to byte address if needed */

	if (count == 1) /* Single block read */

	{

		SD_Write_Block((BYTE*)buff,sector); //Р РЋРЎвЂЎР С�?РЎвЂљР В°Р ВµР С? Р В±Р В»Р С•Р С” Р Р† Р В±РЎС“РЎвЂћР ВµРЎР‚

		count = 0;

	}

	else /* Multiple block read */

	{

	}

	SPI_Release();

	return count ? RES_ERROR : RES_OK;
	return RES_OK;
  /* USER CODE END WRITE */
}
#endif /* _USE_WRITE == 1 */

/**
  * @brief  I/O control operation
  * @param  pdrv: Physical drive number (0..)
  * @param  cmd: Control code
  * @param  *buff: Buffer to send/receive control data
  * @retval DRESULT: Operation result
  */
#if _USE_IOCTL == 1
DRESULT USER_ioctl (
	BYTE pdrv,      /* Physical drive nmuber (0..) */
	BYTE cmd,       /* Control code */
	void *buff      /* Buffer to send/receive control data */
)
{
  /* USER CODE BEGIN IOCTL */
	DRESULT res;// = RES_ERROR;
	//HAL_UART_Transmit(&huart2,(uint8_t*)"USER_ioctl\r\n",12,0x1000);
	//sprintf(str1,"cmd: %d\r\n",cmd);
	//HAL_UART_Transmit(&huart2,(uint8_t*)str1,strlen(str1),0x1000);
	if (pdrv) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	res = RES_ERROR;
	switch (cmd)
	{
		case CTRL_SYNC : /* Flush dirty buffer if present */

		SS_SD_SELECT();

		if (SPI_wait_ready() == 0xFF)

		res = RES_OK;

		break;
	  case GET_SECTOR_SIZE : /* Get sectors on the disk (WORD) */
		*(WORD*)buff = 512;
		res = RES_OK;
		break;
	  default:
		res = RES_PARERR;
	}
	SPI_Release();
	return res;
}
static void float_to_str(const float* mas, char* str);
static void int_to_str(const int16_t* mas, char* str);

void float_to_str(const float* mas, char* str){
	float tmp = (*mas);
	if (tmp<0.0){
		tmp = -tmp;
		sprintf(str, "-%d.%03d ", (int16_t)tmp, (uint16_t)((tmp - (uint16_t)tmp)*1000.));
	}
	else
	{
		sprintf(str, "% d.%03d ", (int16_t)tmp, (uint16_t)((tmp - (uint16_t)tmp)*1000.));
	}
}

void int_to_str(const int16_t* mas, char* str){
	int16_t tmp = *mas;
	if (tmp<0.0){
		tmp = -tmp;
		sprintf(str, "-%d", tmp);
	}
	else
	{
		sprintf(str, "%d", (int16_t)(*mas));
	}
}


FRESULT ReadLongFile(void)
{
  uint16_t i=0, i1=0;
  uint32_t ind=0;
  uint32_t f_size = f_size(&MyFile); //.fsize!!!
  sprintf(str1,"fsize: %lu\r\n",(unsigned long)f_size);
  HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
  ind=0;
  do
  {
    if(f_size<512)
    {
      i1=f_size;
    }
    else
    {
      i1=512;
    }
    f_size-=i1;
    f_lseek(&MyFile,ind);
    f_read(&MyFile,sect,i1,(UINT *)&bytesread);
    for(i=0;i<bytesread;i++)
    {
      HAL_UART_Transmit(muart,sect+i,1,0x1000);
    }
    ind+=i1;
  }
  while(f_size>0);
  HAL_UART_Transmit(muart,(uint8_t*)"\r\n",2,0x1000);
  return FR_OK;
}

DSTATUS sdcard_init(){
	return disk_initialize(SDFatFs.drv);
}

int sdcard_test_stat()
{
  if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
  {
	return 1;
  }
  else
  {
	strcpy(fileInfo.fname, (char *)sect);
	fileInfo.fsize = sizeof(sect);
	result = f_opendir(&dir, "/");
	if (result == FR_OK)
	{
		while(1)
		  {
			result = f_readdir(&dir, &fileInfo);
			if (result==FR_OK && fileInfo.fname[0])
			{
			  fn = fileInfo.fname;
			  if(strlen(fn)) HAL_UART_Transmit(muart,(uint8_t*)fn,strlen(fn),0x1000);
			  else HAL_UART_Transmit(muart,(uint8_t*)fileInfo.fname,strlen((char*)fileInfo.fname),0x1000);
			  if(fileInfo.fattrib&AM_DIR)
			  {
				HAL_UART_Transmit(muart,(uint8_t*)" [DIR]",7,0x1000);
			  }
			}
			else break;
			HAL_UART_Transmit(muart,(uint8_t*)"\r\n",2,0x1000);
		  }

		//-------------------------------------------
		f_getfree("/", &fre_clust, &fs);
		sprintf(str1,"\nfre_clust: %lu\r\n",fre_clust);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1,"n_fatent: %lu\r\n",fs->n_fatent);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1,"fs_csize: %d\r\n",fs->csize);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		tot_sect = (fs->n_fatent - 2) * fs->csize;
		sprintf(str1,"tot_sect: %lu\r\n",tot_sect);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		fre_sect = fre_clust * fs->csize;
		sprintf(str1,"fre_sect: %lu\r\n",fre_sect);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1, "%lu KB total drive space.\r\n%lu KB available.\r\n",
		fre_sect/2, tot_sect/2);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		//-------------------------------------------

	  f_closedir(&dir);
	}
  }
  //FATFS_UnLinkDriver(USERPath);
  HAL_Delay(1000);
  return 0;
}

int sdcard_test_write(const TCHAR* file_name)
{
	FRESULT res;
	char text[]="Everything is good\n";
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	{
		return 1;
	}
	else
	{

	  if(f_open(&MyFile,file_name,FA_OPEN_APPEND|FA_WRITE)!=FR_OK)
	  {
		return 1;
	  }
	  else
	  {
		res=f_write(&MyFile,text,sizeof(text),(void*)&byteswritten);
		if((byteswritten==0)||(res!=FR_OK))
		{
		  return 1;
		}
		f_close(&MyFile);
	  }
	}
	return 0;
}


int sdcard_write_time_to_file(const TCHAR* file_name)
{
	struct time_board time;
	time_get(&time);
	FRESULT res;
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	{
		return 1;
	}
	else
	{
		char elem[20] = "";
		uint8_t sec = time.seconds;
		sprintf(elem, "(%d: %d: % d)   ",time.hours,time.minutes,time.seconds);
		if(f_open(&MyFile,file_name,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
		{
			return 1;
		}
		res=f_write(&MyFile,elem,sizeof(elem),(void*)&byteswritten);
		if((byteswritten==0)||(res!=FR_OK))
		{
			f_close(&MyFile);
			return 1;
		}

	}
	f_close(&MyFile);
	return 0;
}

int sdcard_write_str(const TCHAR* file_name, const TCHAR * tag, uint8_t n)
{
	FRESULT res;
	sdcard_write_time_to_file(file_name);
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	{
		return 1;
	}
	else
	{
		if(f_open(&MyFile,file_name,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
		{
			return 1;
		}
		res=f_write(&MyFile,tag,n,(void*)&byteswritten);
		if((byteswritten==0)||(res!=FR_OK))
		{
			f_close(&MyFile);
			return 1;
		}
		f_close(&MyFile);
	}
	return 0;
}

int sdcard_write_float_array_to_file(const TCHAR* file_name, const float * mas, const uint8_t n)
{
	FRESULT res;
	sdcard_write_time_to_file(file_name);
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	{
		return 1;
	}
	else
	{
		for (uint8_t i = 0; i < n; i++){
			char elem[9] = "";
			float_to_str(mas+i,elem);
			if(i==(n-1)){
				elem[8]='\n';
			}
			if(f_open(&MyFile,file_name,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
			{
				return 1;
			}
			res=f_write(&MyFile,elem,9*sizeof(char),(void*)&byteswritten);
			if((byteswritten==0)||(res!=FR_OK))
			{
				f_close(&MyFile);
				return 1;
			}
			f_close(&MyFile);
		}
	}

	return 0;
}

int sdcard_write_int_array_to_file(const TCHAR* file_name, const int16_t * mas, const uint8_t n)
{
	FRESULT res;
	sdcard_write_time_to_file(file_name);
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	{
		return 1;
	}
	else
	{
		for (uint8_t i = 0; i < n; i++){
			char elem[9] = "";
			int_to_str(mas+i,elem);
			if(i==(n-1)){
				elem[8]='\n';
			}
			if(f_open(&MyFile,file_name,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
			{
				return 1;
			}
			res=f_write(&MyFile,elem,9*sizeof(char),(void*)&byteswritten);
			if((byteswritten==0)||(res!=FR_OK))
			{
				f_close(&MyFile);
				return 1;
			}
			f_close(&MyFile);
		}
	}
	return 0;
}

int sdcard_test_readLongFile(void)
{
	if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
	  {
	    Error_Handler();
	  }
	  else
	  {
		volatile int res = f_open(&MyFile,"lorenIpsum.txt",FA_READ);
	    if(res!=FR_OK)
	    {
	      Error_Handler();
	    }
	    else
	    {
	      ReadLongFile();
	      f_close(&MyFile);
	      HAL_Delay(1000);
	    }
	  }
	return 0;
  /* USER CODE END IOCTL */
}
#endif /* _USE_IOCTL == 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
